# Documentation Survey

Our team is currently collecting feedback on our documentation, this repository included. If you would like to provide feedback, please do so through our survey.

The survey is found [HERE](https://forms.gle/J14byZJRgMkTGzqB9).

# Table of Contents

- [Other Pages](#other-pages)
- [Informatica Integration Cloud Services](#informatica-integration-cloud-services)
- [Benefits](#benefits)
- [Documentation](#documentation)
- [Adding New Content](#adding-new-content)

# Other Pages
* [On-Boarding to IICS](/docs/on-boarding-to-iics.md)
* [Logging in](/docs/logging-in.md)
* [University of Wisconsin Integrator Training](/docs/training/IntegratorTraining.md)
* [Training](/docs/training/training.md)
* [Best Practices And Recommendations](/docs/best-practices/README.md)
* [Integration Tutorials](/docs/tutorials/README.md)
* [Secure Agents](/docs/secure-agent.md)
* [EI Secure Agent](/docs/ei-secure-agent.md)
* [Shared Orgs and Sub Orgs](/docs/shared-org-vs-sub-org.md)
* [Madison and System IICS Orgs](/docs/madison-vs-system-iics.md)
* [Get Help](get-help.md)

# Informatica Integration Cloud Services
Informatica Intelligent Cloud Services (IICS) is a platform that enables data integration between systems and 
applications, including on-premise-to-cloud, cloud-to-cloud, and cloud-to-on-premise.  IICS belongs to a class of 
products called “Integration Platform as a Service” (iPaaS).

Here are some examples of where IICS can be used:

* Syncing data from a database to a third-party cloud service on a nightly basis.
* Processing changes from an API (e.g. CAOS) in real time to update an on-premises system.
* Extracting data from a third-party cloud service to populate a reporting database.

The platform caters to a diverse technical audience including those who may have very little integration experience and 
to those who have advanced development and integration skills.

## Benefits
* Create integrations that move data in real time or on a scheduled basis.
* Pre-built connectors enable reusable integrations to a variety of other services (e.g. Salesforce, Oracle, AWS S3).
* Allows integration of on-premises systems, databases, and cloud-based software as a service (SaaS).
* Design and implement integrations in a web browser using no-code and low-code tools. 
* Run integrations in the cloud or deploy them to your own infrastructure.

The below diagram visualizes some of the iPaaS (IICS) capabilities.

![Using iPaaS to inter-connect different types of systems, that communicate using different protocols or data formats.](images/iPaaS-capabilities.svg)

The diagram source can be found [here](https://www.lucidchart.com/documents/edit/dd614f97-9c7b-4164-8bd7-f9fac442c4c6/0_0).

## Documentation
This repository is the documentation library for DoIT's offering of Informatica Intelligent Cloud Services (IICS). IICS is part of UW's Integration Hub, a collection of service offerings that enable data integration throughout the UW System. You can find more information about these service offerings in the [Integration Catalog](https://integratedata.wisc.edu/integration-catalog/).

Please contact us [integration-platform@doit.wisc.edu](mailto:integration-platform@doit.wisc.edu) if you need additional help or if you weren't able to find answers to your questions in the documentation.
Merge requests are welcome if you have suggestions for improving the documentation, or you may send us an email if you prefer communicating directly.

## Adding New Content
Use [template.md](template.md) to create new content.
