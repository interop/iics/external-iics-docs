# Integrator Training

UW Madison currently provides its personell with a self-paced Canvas course located at [https://go.wisc.edu/integratortraining](https://go.wisc.edu/integratortraining) that provides integrators with the prerequisite knowledge needed to use IICS, as well as a series of instructions to create two end-to-end practice integrations (one in CDI which pulls information directly from an API, and one in CAI which uses webhooks).

This course is currently only available to UW-Madison personell. For other users, there are several alternatives:
- Look at [Our Documentation's Set of Tutorials](/docs/tutorials/README.md)
- Go through the [Official Informatica Training](/docs/training/training.md)