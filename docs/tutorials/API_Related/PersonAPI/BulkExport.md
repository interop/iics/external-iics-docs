# Bulk Export Feature of Person API

The [Bulk Export](https://developer.wisc.edu/person-api/exports) feature of the Person API allows users to request either a full export of all users in the Person API or filter this body of data down to their specifications. The process includes a cascade of POST and GET requests to the OAuth and Person APIs, ending with an external link to retrieve the requested data.

The Integration Team has created a Process with Informatica's Cloud Application Integration (CAI) that can be cloned and adjusted as needed. This Process template and all of its associated objects live in the Project Folder "Templates" and sub-folder "Bulk_Export" in the TEST IICS environment.

![ProjectFolder](images/ProjectFolder.png)

# Understanding This Template

![TheProcess](images/Process.png)

Listed below are the steps we took to create this Process. We hope that through reading this, you will understand how to adjust the template for your own use as well as learn how the Export function for the Person API works.

If you would like a comprehensive guide to creating a full process with its own Service Connectors and App Connections, please consider following [Section 8](https://canvas.wisc.edu/courses/378847/pages/8-dot-a-introducing-webhooks) of the Canvas Course we created for consuming Webhooks (another great and useful [Person API feature](https://developer.wisc.edu/person-api/webhooks)).

### Service Connectors

We need two service connectors for this Process. One for the OAuth API and another for the Person API. In our template we use the MOCK Person API because we don't want to mishandle any real person data. The OAuth Service connector provides the access token used for authenticating with the (Mock) Person API to request the export and retrieve data.

The OAuth Service Connector is set up exactly like [this Canvas tutorial](https://canvas.wisc.edu/courses/378847/pages/8-dot-b-iics-service-connectors), however the Mock Person API is set up slightly different.  Instead of adjusting settings within the Get Person action, we instead focus on 3 of the last 4 actions within the imported API Spec.

![Actions](images/ActionsServConn.png)

1) Adjustments to this Service Connector include adding token Authorization to the Binding for both "create_export" and "get_exports" Actions. 
2) We also needed to find a way to pass dynamic URLs through the cascading calls. 

    a. Find the defined links output in create_export POST Action.

    ![linksOut](images/linksOut.png)

    b. Define a new input field in get_exports to pass the link from create_export. 

    ![URLinput](images/URLinput.png)

    c. Add the URL variable to the Binding tab for get_exports.

    ![URLvariable](images/URLvariable.png)

    d. Add exportUrl and status as Output fields in get_exports (these are returned data points from the GET call, but we need to add them to Output for IICS to use them within the Process we create later).

    ![exportsOutput](images/ExportsOut.png)

    e. In get_export (singular, not plural like above), add an Input field for "export_URL" as well as define it in the Binding tab.

    ![ExportInput](images/ExportInput.png)
    ![ExportUrlvar](images/ExportUrlvar.png)

    f. Later in the Process mapping, we will use these defined dynamic URLs in get_exports and get_export respectively:

    ![IntermediaryURL](images/intermedURL.png)
    ![MegaURL](images/ProcMegaUrl.png)


3) Find a way for IICS to pass a POST body that the Person API would accept.
    
    a. We deleted the existing "data" input field for create_export and replaced it with body. The "test with" section holds some text that works in Test Results, however we had to configure this differently within the Process.

    ![exportbody](images/exportBody.png)

    b. Added Custom Binding Type and added the body variable.

    ![BodyVar](images/BodyVar.png)

### App Connections

App Connections are generally very straight forward and are like the envelope for the contents of their assigned Service Connectors. We created one each for the OAuth and Mock Person API Service Connectors, only needing to add our credentials to the OAuth App Connection after they were created.

A third App Connection is needed for this Process, however, and it does NOT need to be assigned to a Service Connector. This one will be the equivalent of a Connection in CDI and will write our eventual extract payload into a file on the Secure Agent in a folder we've assigned it to.

![AppConWriter1](images/AppConWriter1.png)

![AppConWriter2](images/AppConWriter2.png)

### The Process

![TheProcess](images/Process.png)

Here is how the Process works:

1) The trigger to start the Process is that it will be called in a CDI task flow. Within the Start tile, we assign 3 Temp Fields:
    - JsonPayload  with the type Text
    - XMLPayload   with the type XML
    - tmp_filewrite   with Custom type of pointing to our File Writing App Connection
2) The Process then sends a Post request to the OAuth API for an access token (good for approximately 5 minutes).
3) The access token is used in the Create Export POST call and we retrieve the dynamic link with a id number at the end for the next step.
4) The Get Pending tile and "Is it Pending" tile takes that dynamic link and checks to see if the status is "Pending" (which it would be with large exports) or "Complete". If it's Pending, we wait 15 seconds and try again over and over until it becomes "Complete" or something else.
5) If the status is anything except Pending or Complete, we end the process.
6) If the status is Complete, we retrieve the Mega URL from the response. This Mega URL is a google link with the payload of our data and is only good for about 3 minutes. 
7) Due to one of the hurdles we will explain in the next section, we now have to translate that payload from our last step into XML, then into JSON.
8) Once we have the final form of our payload in JSON, we assign properties to the "tmp_filewrite" variable (from step 1).

![finalform](images/finalform.png)

9) Finally we have the FileWriter Service tile that takes the tmp_filewrite and makes a file on the Secure Agent for us to later grab with a CDI task flow.

![FinalPayload](images/FinalPayload.png)

### Hurdles

Here is a walkthrough of some hurdles we faced with this Process and how we overcame them. 

1) As shown in the Service Connector section, we had some difficulty passing dynamic URLs and overcame that mostly within the Service Connector Actions settings instead of adjusting the Process. After creating all the definitions and settings within the Service Connector, we then mapped those definitions in the "Get Pending" and "Get Export Mega URL" tiles (please see the screenshots above, in section 2.f. of Service Connector).
2) Passing a recognizable Body in the POST request for create_export. This was a matter of syntax and wording.  We had to transform one of the Input fields from "data" to "body" in the Service Connector (as mentioned in the Service Connector section, step 3) and then within the Process we made that input field value "Formula" and wrote this:

![bodyforumula1](images/BodyFormula1.png)
![bodyforumula2](images/BodyFormula2.png)

3) One of the final hurdles was getting the payload written in JSON as a .json file. We followed these steps to get our payload into a .json file that is readable by IICS in a CDI process:
    
    a. Add two Temp fields in the Start tile.  One is for Json and the other for XML.

    ![tmpJSONXML](images/tmpJsonXml.png)   
    
    b. Add an Assignment tile and assign XMLPayload to the incoming payload with the parseXML utility of the Formula function.

    ![XMLpayload](images/xmlpayload.png)

    c. Add an Assignment tile and assign JsonPayload to the XMLPayload we just translated using the toJSON utility of the Formula function.

    ![jsonpayload](images/jsonpayload.png)

    d. Finally we assign the JsonPayload to the tmp_filewrite variable along with assigning a File Name with .json extension and Content Format of PlainText.  There *IS* an option to choose JSON instead of PlainText, however we found this leave the end file blank instead of containing our payload.

    ![tmpwrite](images/tmpwrite.png)