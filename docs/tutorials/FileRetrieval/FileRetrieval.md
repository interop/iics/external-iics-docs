# Retrieve a file from the Secure Agent

The Secure Agent is a Linux-based virtual machine that lives in the AWS cloud space. Since it is behind multiple virtual layers, it may be difficult to retrieve files directly off of it.  We recommend a small addition to your mapping for regular retrieval of a file, or a small independent mapping for one-off retrieval of files.

To accomplish this, please have a Box account or S3 bucket with a connector set up in IICS to that storage space. Below is an example of an independent mapping to retrieve a log file.

### Mapping from Secure Agent to your storage connection
1. Login to IICS.
2. Choose _Data Integration_ in the top menu dropdown.
3. Choose _New..._ in the left menu.
4. Choose _Mappings_ in the left pane of the pop-up window, then choose _Mapping_ in the body of the window. Click _Create_.

![Create Mapping](images/Create1.png "Create Mapping")

5. Click on the _Source_ box in the mapping, then _Source_ in the _Properties_ section at the bottom of the window.
6. In the _Connection_ drop menu, select an existing Flat File connection to the Secure Agent. If you don't have an existing connector for your team, you may use _IntegrationTeamFlatFileTemp_, which leads to the temp folder on the Secure Agent.
    - If the file you need isn't in the temp folder on the secure agent, you will need to create a Flat File connector that leads to the appropriate directory.
7. Select _Single Object_ in _Source Type_ and then click the _Select..._ button for _Object_.
8. In the pop-up window, browse through the file tree to the file you wish to retrieve off of the secure agent.

![Source](images/Source2.png "Source Window")

9. Select the _Target_ box in the mapping, then select _Target_ in the _Properties_ section at the bottom of the window.
10. Select your _Connection_ in the drop menu. We recommend a Box account or an S3 bucket for this purpose.
11. _Target Type_ should be Single Object.
12. Click the _Select..._ button for _Object_. In the pop-up window, we recommend selecting the radio for _Create New at Runtime_. If this is a recurring task in your existing mapping, you may wish to select the _Existing_ radio for overwriting with each run.
13. Create a name for your file and click _OK_.

![CreateNew](images/CreateNew3.png "Create New")

14. Run your mapping and verify file is now in your storage space.
