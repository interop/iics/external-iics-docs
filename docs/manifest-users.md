# Provisioning and Managing users in Manifest
- [Adding Users to Manifest](#adding-users-to-manifest)
- [Creating a New Manifest Group](#creating-a-new-manifest-group)
- [Removing Users from Manifest Groups](#creating-a-new-manifest-group)
- [Managing Users as an IICS Sub-Org Admin](#managing-users-as-an-iics-sub-org-admin)

# Adding Users to Manifest

DoIT EI uses Manifest groups to give teams access to the UW-Madison IICS environment.

1. Add your team member(s) to the manifest group. Please read the documentation on [Managing Manifest Group Members](https://kb.wisc.edu/page.php?id=25882).
2. Have your team members log into test and production. Please read the documentation on [Logging in to IICS](/docs/logging-in.md).
3. Notify our team at integration-platform@doit.wisc.edu once all group members have logged in to both orgs at least once. No further action on your part is required after this. We will need to provision your users to their user group within IICS manually. This must be done by a member of the Integration Team or another IICS admin.

# Creating a New Manifest Group

Manifest groups do not automatically have access to IICS, as these permissions must be provisioned by the Integration Team.

1. Create a Manifest Folder / Choose an existing Manifest Folder. A Manifest Folder is a prerequisite to creating a Manifest Group. If you do not have (or wish to use) an existing Manifest Folder, please refer to documentation on [How to Create a Manifest Folder](https://kb.wisc.edu/page.php?id=27783).
2. Create a Manifest Group. Follow this documentation on how to [Create a Group](https://kb.wisc.edu/page.php?id=25878).
    * To keep the group easily identified, we recommend prefixing the name of the Group with the name of your department (e.g. "DoIT-AIS-Enterprise-Integration-IICS"). See the documentation on [Manifest Group and Folder Naming Advice and Philosophy](https://kb.wisc.edu/page.php?id=84828).
3. Grant Read and View privileges for your new Manifest Group to our team's group, uw:org:ais:ais-admins. This will allow our team to view the names of members in your group so that we know to set up their accounts in IICS. [Follow These Instructions](https://kb.wisc.edu/25880).
4. Email our team at integration-platform@doit.wisc.edu with the Manifest group path (e.g. uw:org:ais:DoIT-AIS-Enterprise-Integration-IICS). We will be able to permit your team members access to IICS.
    * We will also provide you with your IICS User Group name which you will use to restrict access to your team's assets in IICS. Please refer to [Documentation on User Groups](/docs/best-practices/shared-environment.md).
5. Add users to the Manifest Group. View the [Instructions Above](#adding-users-to-manifest).
6. Wait for a response from our team. Once we have provided your Manifest group access to IICS we will let you know.

# Removing Users from Manifest Groups

1. To remove an IICS user from your IICS User Group, delete the member in your Manifest Group. Please read the document on [Managing Manifest Group Members](https://kb.wisc.edu/page.php?id=25882).
2. Once you have removed the required members email integration-platform@doit.wisc.edu with the IICS User Group name, and the NetId of the member to remove. DoIT EI will notify you when we have removed the user from your IICS User Group.

# Managing Users as an IICS Sub-Org Admin

If you are the admin of an IICS Sub-Org and would like to add, manage, or remove users, the process is mostly the same as for an end user of a non Sub-Org. The above instructions still mostly apply. You will need to manage some parts of this process on your own however.

- When adding users to your organization, you will need to do so in a method automated through SAML. Do not add users directly through the IICS Admin Portal.
- You will still need to grant Read and View priveleges for your Manifest Group to our team as per the original instructions, and contact us to provision access to the Sub-Org.
- You will need to create a User Group within your Sub-Org to manage permissions. For information on how to do this, please refer to the [IICS Documentation on User Groups](https://docs.informatica.com/data-integration/b2b-data-exchange/10-2-3/administrator-guide/user-policies/user-groups.html).
- After a user logs in (on both test and prod induvidually), you will have to add them to the User Group corresponding to their Manifest Group manually.
- When removing a user from the Manifest Group, you must also remove them from the IICS User Group manually.
  - If you want to remove the user from the sub-org entirely you will need to disable them. Please refer to [IICS Documentation on Disabling Users](https://docs.informatica.com/integration-cloud/cloud-platform/current-version/user-administration/users/disabling-a-user.html).
