# Working in Multiple Organizations

The UW Madison IICS instance has two "orgs", Test (representing experimental assets and pieces of integrations that are still in production) and Prod (representing the assets used for active production integrations). Keeping this distinction is a responsibility we share with the Integrators that use IICS, and this page will contain all relevant information on what you should do to maintain the purpose of each org.

## Building Integrations in Test

When building an Integration's initial assets in IICS, it should always be built in Test first. This will make sure that assets representing the more experimental and exploratory parts of development aren't making the Prod org unintuitive to navigate and maintain.

Once an Integration's functionality has been confirmed, it can freely be moved to Production.

## Asset Migration

When moving IICS assets from one org to another (most commonly from Test to Production), there are several methods that can be used, each described below.

### Import and Export

IICS has built in functionality for the export and import of assets between organizations. Please refer to the [Informatica Knowledge Base Article](https://knowledge.informatica.com/s/article/523654?language=en_US) on the subject.

### IICS Assets Tool

Enterprise Integration has created a tool to synchronize assets between Test and Production, located at [This Repository](https://git.doit.wisc.edu/interop/iics/iics-assets/-/tree/master). Please contact the DoIT Integration Team at [integration-platform@doit.wisc.edu](mailto:integration-platform@doit.wisc.edu) if you would like to access it.

## Editing Production Integrations

Once an Integration has been moved to Production, that does not necessarily mean it has been completed for good. A combination of maintenace, unforseen bugs, and new requirements may result in an Integration and its assets requiring edits even after its listed completion.

In this case, you should do all required edits on the assets within the Test Org first, before mirroring these changes in Prod (either through a manual export or by manually editing the assets to match).

If this is unviable, then at minimum the user should create a copy of the asset they wish to edit before making any edits, as to ensure the original remains in the case of any errors being introduced in the edits.
