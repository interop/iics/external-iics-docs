# AWS S3 Bucket Connector v2
The Integration Team uses this connector regularly and we highly recommend it if it fits your needs.  We find it very reliable, secure, and versatile.  

Please be aware that IICS has two Amazon S3 connectors and we use the v2 connector, mainly due to it's support of flat files.  [Here is a good FAQ](https://knowledge.informatica.com/s/article/531836?language=en_US) of the differences between the two connectors.

## Use Case Examples

1) You do not have access to an SFTP server and need a place for a large file to be dropped off and over-written daily after being processed by IICS.
2) IICS will connect to a database and pull data from it, create a .csv file, and will then write it to the S3 bucket. After this file is created, you can make a second mapping to further process the data and write it to either another folder in the S3 bucket, or to a cloud application like Salesforce. 

Please note that [S3 buckets are great for mappings and mapping tasks](https://docs.informatica.com/integration-cloud/data-integration-connectors/current-version/amazon-s3-v2-connector/introduction-to-amazon-s3-v2-connector/amazon-s3-v2-connector-assets.html), however they are not yet supported with synchronization tasks.

## S3 Bucket Set up

For set up of a team AWS account, creation of an S3 bucket within it, and an IAM user within the account (to act as a service user for your IICS connector), please contact the [Public Cloud Team](https://kb.wisc.edu/page.php?id=109785).  It may be helpful to show them [this IICS article](https://docs.informatica.com/integration-cloud/data-integration-connectors/h2l/1199-configuring-iam-authentication-for-amazon-s3-and-amazon-s3-/configuring-iam-authentication-for-amazon-s3-and-amazon-s3-v2-co.html) so that they know the specs to add into the account in order for the IICS connector to work successfully.

If you do not want an AWS account for your team or are looking to use an S3 bucket for a one-off, low bandwidth integration, the Integration Team may be able to create an S3 bucket and a dedicated IAM user within our team's account for you.  Please consult with us by emailing integration-platform@doit.wisc.edu to see if we can accommodate you in this way. Please note we would be unable to offer you console access to view the contents of the bucket, it would be used as intermediary storage.

You will need these things to move on to the next step:
 - An S3 Bucket (the name of it).
 - The credentials of an IAM user within the AWS account that the S3 bucket lives in.
   - this user will need to have permissions to access the S3 bucket
   - this user may be named "iics-agent" or something similar within AWS, but it will have an **Access Key** and **Secret Key** that are long strings of characters that are associated with it, and those two things are what IICS needs.
 - Region name of where the S3 bucket lives.

## Connector Example
Please see screenshot below of a working S3 v2 connector.

The Access Key and Secret key, as mentioned above, is from your IAM user created in your AWS account.  The folder path is simply the name of your S3 bucket.  The last thing for the connection to work properly is the Region name that the S3 bucket lives in.

![S3_Connector](images/S3_v2_connector.png)