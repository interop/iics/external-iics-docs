# Working In a Shared Environment

UW-Madison operates under a single shared IICS org for each environment (test and production).
This means that you might see other objects in IICS, such as projects, connections, and schedules, that belong to other areas of the university.

## Using Asset Permissions to Control Access with User Groups

When you create a new project, make sure you edit the permissions to limit who can view and change the assets within the project.
**If no permissions are configured on a project or the assets within it, anybody who has access to IICS will be able to view, change, run, and delete the contents of the project. If ONE specified user has permissions on the project or asset, NO OTHER PERSON can view, change, run or delete the contents.**
Use the group that was sent when you were granted access to IICS to control who can access the contents of the project.

1. Right-click on a project folder and click "Permissions..." 
    - ![Project right-click menu](images/right-click-permissions.png)
1. Click "Add" to add bring up the group selection menu. 
    - ![Project permissions menu without group](images/add-group.png)
1. Select the group for your team and click "Add". 
    - ![Group selection menu](images/select-group.png)
1. Click the check boxes for Read, Update, Delete, Execute, and Change Permission. 
    - ![Project permissions menu with permissions applied.](images/apply-permissions.png)
1. Click Save.

## Using User Groups to Simplify Permissions

User Groups are collections of multiple users that can be utilized to simplify the process of setting up asset permissions. Teams integrating in IICS will have a User Group set up for them and be placed in it. When setting asset permissions, it is protocol to set the permissions by group rather than by individual user, as to make deprovisioning of users smoother and ensure assets are acessible by the remaining team members.

If you do not believe your group has a user group set up, contact the Integration Team at [integration-platform@doit.wisc.edu](mailto:integration-platform@doit.wisc.edu). For more information on the concept of User Groups, check the [Relevant Informatica Documentation](https://docs.informatica.com/cloud-common-services/administrator/current-version/user-administration/user-groups.html).

## Namespace Projects With Your Group Name

Even though you might not have any permissions on a project, you are still able to see that the project exists when exploring all projects.
To help organize the constantly growing list of projects, we recommend that you to prefix your group name before the name of the project: `{group name}-{project name}`

Example: "DoIT_AIS_Enterprise_Integration-Salesforce_Integrations"

This structure allows you to search your group name in the project explorer to only see a list of your group's projects.
You can create subfolders within a project if further organization/hierarchy is needed.

Your group name was sent when you were granted access to IICS and you can find the name of your group by editing the permissions for a project.
