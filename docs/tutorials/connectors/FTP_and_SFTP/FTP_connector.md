# FTP/SFTP Connector

This connector can be used for an FTP connection as well as an SFTP connection. The difference between the two is port numbers: 21 for FTP and 22 for SFTP. This is determined by the FTP server you're connecting to.

# Requirements

* A FTP or SFTP server's settings 
    * Hostname
    * User credentials
    * Port number
    * Directory path on the FTP/SFTP server for delivery, if not to the root directory
* A local directory on the Secure Agent you are creating the mapping on. If your team does not have a folder dedicated for your work, you can reach out to the [Integration Team](mailto:integration-platform@doit.wisc.edu) to request one, however using the /tmp directory works fine for this Connector as the file isn't retained on the Secure Agent after successful completion.

# Connector

1) In Administrator > Connections, select "New Connection in the upper right corner of your browser.
2) Make sure to fill out these settings:
    - Name: Please follow our [naming guidelines](/docs/best-practices/naming.md).
    - Type: FTP/SFTP
    - Runtime Environment: The Secure Agent you typically use.
    - User Name:  The user created for you on the FTP/SFTP server.
    - Password:  The above user's password.
    - Host:  The FTP/SFTP server hostname
    - Port:  This will toggle to either 21 or 22, depending on if you select "This is a secure FTP connection". The FTP/SFTP server administrator should give this information to you with the hostname and credentials.
    - Local Directory:  This is either /tmp or the folder path provided to you by the Integration Team after you've requested a team folder from them.
    - Remote Directory:  If delivering to the root of the file structure on the FTP/SFTP server, you will need to input "/" in this field. If you are delivering to a specific directory on the server, you will need to lead with the "/" (for example: /UAT/data).
    - Code Page: The most commonly used format is UTF-8, so we recommend this unless the FTP/SFTP administrators or platform consuming this data requires something else.

![SFTPconnector](images/sftp_settings.png)

# Mapping

If using this connector as a source, there needs to be a file ready to move in the FTP/SFTP server before you start.

1) In the Data Integration section of IICS, browse to your Project Folder, then select "New..." in the upper right of your browser.
2) Select Mappings > Mapping, then click the "Create" button.
3) Create a name for your Mapping.
4) In the Source tile, in Properties > Source, select the connector we created in the section above.
    - Select Source Type: Single Object
    - Object: Click on the "Select..." button and then select your file you would like to start your Integration with.

If using this FTP/SFTP connector as a target, you can select "Create New at Runtime" as the Object. If you do so, you will need to create the Object Name with the file extention included.

# References

* [IICS KB on the FTP/SFTP Connector](https://docs.informatica.com/integration-cloud/data-integration/current-version/data-integration-connections/ftp-sftp-connections.html)