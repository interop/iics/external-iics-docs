# Connections involving SSH key pair authentication

Using SSH key pair authentication instead of username and password credentials requires an in-depth set up on the Secure Agent. Faced with this, the Integration Team asks that you contact us to generate the key pair on the EI Secure Agent before you create the connector that you need. We can work directly with the system administrators of the server you're communicating with to exchange the public key and discuss with you where it's located within the Secure Agent for further set up of the IICS connector.

If you administer your department's separate Secure Agent, please let us know if you require assistance with generation of the key pair as well as adjusting settings on an IICS connector.

We can be reached via email at integration-platform@doit.wisc.edu
