# On-Boarding to IICS

In order to get access to Informatica Intelligent Cloud Services (IICS), please [email the DoIT Integration Platform Team](mailto:integration-platform@doit.wisc.edu) with answers to the following questions:

- What is your use case? What integration(s) are you wanting to have implemented, and what business need are you trying to satisfy?
- Are there any timelines you need to follow? Are there any deadlines we should know about?
- Do you have staffing available to implement and support the integrations? If so, proceed with the following questions. Otherwise, let us know if you're looking to have integrations implemented and supported for you. We can connect you with professional services, or in some cases, take on the work ourselves.
    - Will you be using a shared org, or will you require a sub-org? For more information on this question, please see [this document](/docs/shared-org-vs-sub-org.md). If you aren't sure, let us know and we can discuss this further.
    - A Secure Agent is required to run integrations. Are you able to manage you own secure agent, or do you need the Integration Platform to provide one for you? See [this document](/docs/secure-agent.md) for more information on Secure Agents. If you aren't sure if you'll need your own secure agent, let us know.

Someone will get back to you within 3 business days with next steps, including instructions to get access.
Depending on your use case, we might schedule a meeting to discuss your use case further and decide on next steps.

## Access Control

Access to IICS is controlled through Manifest. Please refer to [This Documentation](/docs/manifest-users.md) for more information.

## Next Steps

After getting access to IICS, you will be able to work through our various training options and start implementing your integration(s). 

- [Log In to IICS](/docs/logging-in.md)
- Follow the [University of Wisconsin Integrator Training](/docs/training/IntegratorTraining.md)
- Look at [Several Other Integration Tutorials](/docs/tutorials/README.md)
- Go through the [Official Informatica Training](/docs/training/training.md)

Our team will work on setting up any additional connectors, sub-orgs, or secure agent licenses, if applicable to your use case.
Informatica also provides documentation to get started (e.g. [getting started guide for Cloud Application Integration (CAI)](https://knowledge.informatica.com/s/article/DOC-17653?language=en_US)).
