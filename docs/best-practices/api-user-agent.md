# API User Agent

The User Agent is a string included in the header of API calls, used by applications to identify who or what is sending the request. This can represent anything from a user accessing a service via browser to a scraping bot. They can be used by a website to disallow certain connection requests.

By default, any request sent using a Rest Connector in CDI has a User Agent header value of 'Java/1.8.0_345'. If a different header value is provided in the request mapping, it will be used instead.

Of note is that in a header, User Agent is stylized as 'User-Agent'.

## Identifying User Agent Errors

Errors related to user agent can happen in any type of request, whether it be posting data to a site or retrieving it. Errors related to User Agent most often are marked with an error code of 1010.

If these errors are related to Cloudflare, you may see messages such as 'The owner of this website (URL) has banned your access based on your browser's signature', and 'Access denied | (URL) used Cloudflare to restrict access'. These errors can pop up after extended periods without issue, due to Cloudflare's dynamic nature.

## Solving User Agent Errors

If the User Agent is disallowed and you have contact with administration or service related to the API you are using, it is recommended to contact them and ask them to allow the specific User Agent to use the api.

If you are unable to solve this issue via communication, you will have to edit your IICS assets to rectify the issue. When selecting a new User Agent, we recommend you use the string 'PostmanRuntime/7.43.0' - this user agent is used by default by the current (as of January 2025) version of Postman, and generally works.

Different IICS assets using APIs have different methods of setting headers. There are two common options - Rest v2 as a Mapping Source, or Web Service Transformations using a Business Service Asset.

### Rest v2 Source - Editing Request Messages

If you are accessing the API through a Source in your mapping, you will be able to configure the Request Message. This message can be found by going to the Source tab of the Source block in the mapping, opening up the Request Options dropdown, and selecting the button Labelled 'Request Message: Configure'.

If your request message looks like:
```
{
  "Authorization" : "Bearer ExampleToken",
  "Api-Key" : "ExampleKey"
}
```

Then a properly reconfigured request message would look like:
```
{
  "Authorization" : "Bearer ExampleToken",
  "Api-Key" : "ExampleKey",
  "User-Agent" : "PostmanRuntime/7.43.0"
}
```

Official Informatica documentation on Request Messages can be found [On This Webpage](https://docs.informatica.com/integration-cloud/data-integration-connectors/current-version/rest-v2-connector/rest-v2-operations/rest-v2-source-operations/configuring-a-request-using-request-message-editor.html).

### Web Service Transformation - Editing Swagger Files

If your error is present in a Web Service transformation, you will have to edit the Swagger file to configure the User Agent to a custom value.

Header files are found in the Parameters field of an operation. More information on how Swagger file operations are constructed can be found [Here](https://swagger.io/docs/specification/v3_0/paths-and-operations/), and more information on how Parameters specifically are constructed can be found [Here](https://swagger.io/docs/specification/v3_0/describing-parameters/).

The biggest thing of note is that the Header Parameter you would be adding would look like this:
```
{
    "name" : "User-Agent",
    "in" : "header",
    "description" : null,
    "required" : false,
    "type" : "string"
}
```

And an example operation with this Parameter would look like:
```
"get" : {
    "tags" : [ "ExampleTags" ],
    "summary" : null,
    "description" : null,
    "operationId" : "ExampleID",
    "produces" : [ "application/json" ],
    "consumes" : [ "application/json" ],
    "parameters" : [ {
        "name" : "ExampleOtherHeader",
        "in" : "query",
        "description" : null,
        "required" : false,
        "type" : "number",
        "format" : "int32"
    }, {
        "name" : "User-Agent",
        "in" : "header",
        "description" : null,
        "required" : false,
        "type" : "string"
    } ],
    "responses" : {
        "200" : {
        "description" : "example",
        "schema" : {
            "$ref" : "#/example"
        }
        }
    }
    }
}
```

Do note that when editing a swagger used in a Business Service, if you want it to refresh to the new metadata you will have to select the 'Dynamic Refresh' option in said Business Service. You will not have to run the mapping to have it refresh. More information on this can be found [On This Webpage](https://knowledge.informatica.com/s/article/000213647?language=en_US).

Once you have updated your swagger file, and updated your Business Service to use Dynamic Refresh, you will be able to update your Mapping. In the Request Mapping section of your transformation, there should be a new field to map labelled 'User-Agent'. Using an Expression to create a field containing an expression of 'PostmanRuntime/7.43.0' and then mapping it to the 'User-Agent' field in the Web Services transformation will result in your new header being passed through. More information on Web Service Transformations can be found [Here](https://docs.informatica.com/integration-cloud/data-integration-connectors/current-version/rest-v2-connector/rest-v2-operations/rest-v2-source-operations/configuring-a-request-using-request-message-editor.html).