# Logging Into IICS

Use these links to log into the Shared IICS Orgs.
IICS uses NetID Login for authentication.
Test IICS uses the NetID Login QA environment, which is connected to production credentials.

- [test](https://dm-us.informaticacloud.com/ma/sso/fu0Dw88PzqRdTYPctT73QJ)
- [prod](https://dm-us.informaticacloud.com/ma/sso/732dcgB8WwTgRubL1mFU8R)

If you use your own sub-org, please use the links provided to you when your org was provisioned.

## Accessing Multiple IICS Orgs/Environments Simultaneously

It is not possible to access multiple IICS Orgs/Environments in a single browser session as login information is cached during the session. The following methods can be used if you require access to multiple Orgs/Environments:
1. **Use multiple browsers**: Login to one Org/Environment in one browser (eg. Google Chrome), and use another browser (eg. Firefox) to login to another Org/Environment.
1. **Private/Incognito mode**: Login to one Org/Environment in your normal browser window, and use a private/incognito browser window to login to another Org/Environment.

For more information, view [This Informatica Article](https://kb.informatica.com/howto/6/Pages/23/583317.aspx).

## Gaining Access to IICS

Before a user can log in, they must be added to IICS.

For a guide on requesting access to IICS for you or your business unit, refer to our [Onboarding Guide](on-boarding-to-iics.md).

For a guide on provisioning access to IICS to other members of your business unit, refer to our [Manifest User Documentation](manifest-users.md)

## Test or Prod?

If deciding which Org to work with for your current project, reference our documentation on [Working With the Test and Prod IICS Orgs](docs/best-practices/multiple-orgs.md).
