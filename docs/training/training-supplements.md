IICS Training provides clear instructions on how to complete each component of the tutorials, but if you are unable to follow these instructions exactly as given, you may have trouble finding viable workarounds. Supplemental materials based on experiments and tests by various parties who have done IICS training are documented on this page.

#### Secure Agent on Non-Windows Machines

Assuming one is using their own trial organization while going through the tutorials, they will need to set up their own secure agent. All instructions given in IICS Training assume that you are running a secure agent from the desktop of your x86 or x64 windows machine. If one is using a different operating system this can prove troublesome, even more so if one is using a Macbook with an M1 card (or some other alternative chip format). One well-tested solution to this issue that will most likely work in any circumstance is to create a Docker Container inside of an AWS Workspace; [Instructions on this process can be found here](/docs/training/docker-secure-agent.md).

Do note that when using a machine with an M1 chip, you will be unable to successfully run the secure agent outside of AWS, even inside of a virtual machine. This has been tested with a variety of methods and is most likely a product of the nature of the program, something that cannot be worked around.

Assuming one is not using their own trial organization, but rather the shared test org, then they may simply use ei.secureagent.doit.wisc.edu instead. Do note that loading flat files into this secure agent may prove difficult due to its remote nature, and one might want to use an alternative for storing files (such as an S3 bucket).

#### Databases Without Oracle

Myriad labs in IICS request that you use an Oracle database as a source for data. Setting these up can be significantly time consuming. A PostgreSQL database will work fine for this purpose if the proper data is loaded in, however.

The University hosts a test database for IICS purposes. [Information on this database can be found here](https://git.doit.wisc.edu/interop/iics/training-resources/-/tree/main). Several extant tables exist that are equivalents of what would be in an Oracle database. Do note that t you can use a database as a target in IICS, although this never comes up in training.

If you are setting up the database yourself, or wish to create your own tables in the test database, do note that the given database commands are written for Oracle specifically: they will not work for Postgres, and will have to be converted to the other format.

Do note that when using these instructions, Lab 4-1 in CAI may not work properly, which may necessitate skipping it.

#### Understanding Expressions Beyond the Basics

Most labs have you use an expression transformation, and provide you with a block of text to paste somewhere to transform data from one state to another. The labs fail to explain what this text means and how to make your own expressions, however. [A reference to the functions available for use in expressions can be found here](https://docs.informatica.com/integration-cloud/cloud-data-integration/current-version/function-reference/preface.html).

For those with experience in using programming language, an alternative can be found in the [Java Transformation](https://docs.informatica.com/integration-cloud/cloud-data-integration/current-version/transformations/java-transformation.html). Usage of the Expression Transformation is reccomended over this for most use cases however, as it has less overhead and is easier to read.
