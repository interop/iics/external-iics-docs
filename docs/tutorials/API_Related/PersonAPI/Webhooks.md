
# Webhook Endpoint in IICS Cloud Application Integration

Webhooks are a great way to receive notifications for changes to person data without having to consume an entire batch of data to find the changes yourself. This is a feature of the Person API and you can find the knowledgebase article for Webhooks [here](https://developer.wisc.edu/person-api/webhooks).

We have created a full tutorial on subscribing to Person API Webhooks and consuming them with Informatica. You can find this tutorial in Canvas in [Section 8](https://canvas.wisc.edu/courses/378847/pages/8-dot-a-introducing-webhooks) of the full [UW Integrator Training](https://canvas.wisc.edu/courses/378847).  

## References

 - [Here is IICS Documentation](https://docs.informatica.com/integration-cloud/cloud-application-integration/current-version/design/designing-processes/creating-a-process.html) for Cloud Application Integration 
 - [Here is the API Team's Webhook Documentation](https://developer.wisc.edu/docs/person-api/1/routes/people/webhooks/get) for a full overview on how to subscribe to Webhooks and receive a validation token for the Person API.

