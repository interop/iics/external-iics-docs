### Contents

- [Introduction](#introduction)
- [Prerequesites](#prerequisites)
- [Connections](#connections)
- [Usage as Source in Mappings](#usage-as-source-in-mappings)
- [Creating Your Own Swagger Files](#creating-your-own-swagger-files)
- [Important Notes](#important-notes)

### Introduction

This tutorial demonstrates how to create a connection within Informatica Intelligent Cloud Services Cloud Data Integration (IICS CDI) using the RESTv2 connector to connect to the [Mock Person API](https://developer.wisc.edu/docs/mock-person-api/1/overview). In production, you will want to use the non-Mock [Person API](https://developer.wisc.edu/docs/person-api/1/overview). Instructions for requesting access to it are located [Here](https://developer.wisc.edu/person-api/getting-access) if you don't have access already.

### Prerequisites

- [Access to IICS](/docs/on-boarding-to-iics.md)
- [Access to a working IICS secure agent](/docs/secure-agent.md)
- [An App within the UW-Madison Developer Portal](https://developer.wisc.edu/get-started)

### Connections

1. Open a browser window and browse to the [Mock Person API page](https://developer.wisc.edu/docs/person-api/1/routes/people/get). You will need to log in with your NetID. At the very bottom of the page, make note of the Token URL: https<area>://api.wisc.edu/oauth/token

2. In the upper right of the developer.wisc.edu portal, click your name's drop menu and select _Apps_

![Developer API Portal](images/devportallogin.png "Developer API Portal")

3. Select the application connected to your developer account, then browse to the API Keys section. You will need the Key and the Secret on the IICS portal. If you do not have access to this, please follow [these instructions](https://developer.wisc.edu/person-api/getting-access) to gain access to Person API.

![API Keys](images/APIKeys.png "API Keys")

4. [Log into IICS](/docs/logging-in.md) with your NetID.
5. Chose the _Administrator_ tile in the My Services window.
6. Select _Connections_ in the left pane.
7. Select the _New Connection_ button in the upper right of the window.

![IICS Administrator](images/administrator.png "IICS Administrator")

8. In the _Type_ dropdown menu: 
    - Create a _Connection Name_
    - Select _REST V2 (Informatica Cloud)_
    - Select your _Runtime Environment_
    - Select _OAuth 2.0-Client Credentials_ in the drop menu for _Authentication_
    - Paste https<area>://api.wisc.edu/oauth/token in the _Access Token URL_ field (we got this from step 1 above)
    - Paste API and Secret from step 3 into the _Client ID_ and _Client Secret_ fields
    - Click the _Generate Access Token_ button next to the _Access Token_ field and it will auto-populate
    - In the _Swagger File Path_ field, paste https://git.doit.wisc.edu/interop/iics/external-iics-docs/-/raw/master/docs/tutorials/API_Related/PersonAPI/swagger.json (this is a REST v2 OpenAPI specification file that may be different than the one you use in production later. IICS doesn't currently accept V3 and also frequently refers to this OpenAPI specification as a Swagger file.)

![RESTv2 Settings](images/RESTv2settings.png "RESTv2 Settings")

9. Press the _Test Connection_ button in the upper right of the window to verify the connection is successful.
10. If the connection is successful, press the _Save_ button.

### Usage as Source in Mappings

1. To use PersonAPI as a source in a mapping, select the connection you just created as the source. To get a list of all people, select _get people_ as the operation.

![Selection of Mapping Source](images/mapping1.png "Output Group")

2. In order for the source to be used, you must go to the Request Options of the source and Configure the Request Message

![Request Options](images/mapping2.png "Request Options")

3. No request message is required for this specific API function to work, although IICS requires one. Simply add '{}'. This will throw an error message while validating, but will still result in successful access to the data once the mapping is run.

![Request Message](images/mapping3.png "Request Message")

4. Go to the Field Mapping tab within the source, and click the boxes on the left for each item you would like to map. Different dropdowns will show up on the right, containing the values you have mapped. These are groups that will be used in the next step. Select _data_.

![Field Mapping](images/mapping4.png "Field Mapping")

5. Once you have done Field Mapping, attempt to connect your source to any transformation. This will create a selection menu of several values, each corresponding to the groups seen in the Field Mapping. In this specific case, "data" contains all of the relevant data related to a Student.

![Output Group](images/mapping5.png "Output Group")

### Creating Your Own Swagger Files

To perform different operations, you will need to create and host your own Swagger files in the OpenAPI 2.0 Format. IICS provides a tool for the creation of Swagger files, although these can only have one associated operation.

1. [Log into IICS](/docs/logging-in.md) with your NetID.
2. Chose the _Administrator_ tile in the My Services window.
3. Select _Swagger Files_ on the left pane.
4. Select the _New_ button in the upper right of the window.
5. Set the _Runtime Environment_ of your Swagger File.
6. Set the _URL_ as https<area>://mock.api.wisc.edu and the _API Path_ as /people. Leave the _API Base Path_ blank.
    - Note that while there is authentication for the API, you can leave the _Authentication_ as blank in the swagger file, as authorizing requests is handled on the connector level.
7. Set the _Operation ID_ to the name of the operation you want to use. This can be found in the API spec.
    - Also set the _Verb_ to the one matching the operation. For operations which involve receiving data, this will usually be GET.
8. Obtain a response file from PersonAPI for the operation you have selected. This can easily be done using [Postman](https://www.postman.com/).
9. Upload this response file in the _Json Response File_ field.
10. Edit the _Headers_ and _Query Parameters_ fields as needed.

![Swagger File Creation](images/SwaggerCreation.png "Swagger File Creation")

11. Save your Swagger file. Once you have saved it you can no longer edit it, and must repeat the process from step 5 to make any changes.
12. Navigate back to _Swagger Files_ on the left pane.
13. Locate the Swagger File you just created in the list below.
14. Press the Download button to download the file to your personal computer.
15. Host the file somewhere that can be publicly accessed, such as a public Gitlab repository.

Once this process is complete, you will be able to use the link to your Swagger file in the _Swagger File Path_ section of the RESTv2 Connector.

![Swagger File Download](images/SwaggerCreation.png "Swagger File Download")

### Important Notes

- The RESTv2 Connector does not accept swagger files of the OpenAPI3.0 specification (which PersonAPI uses), so the Swagger files must be created using the IICS tool.
- PersonAPI has a strict limit on the amount of requests you can make per minute. Using operations besides Get People may not be feasible in the context of mass data load.
