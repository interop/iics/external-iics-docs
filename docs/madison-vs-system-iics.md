# Madison and System IICS Orgs

IICS is available to all UW System institutions, but UW-Madison has its own IICS org and sub-orgs, funded by and intended for UW-Madison integrators. This document will help you determine if you should use the Madison IICS orgs or the UW System IICS orgs.

Both Madison and System orgs have sub-orgs (or child-orgs) for business units or institutions that have specific needs beyond what's provided in the shared environments. See [Shared Orgs and Sub-Orgs](/docs/shared-org-vs-sub-org.md) for more information on sub-orgs.

## Considerations

- IICS is a tool used for data integration agnostic of source and target; it has no inherent data present in any of the orgs.
- There are no functional differences between Madison and System IICS orgs, and both are licensed for equivalent connectors.
- The [Secure Agents](/docs/secure-agent.md) associated with each org can be made to connect to any source or target with appropriate firewall changes.
- Madison's shared IICS org and the support team that manages it are centrally funded for UW-Madison integrators and use cases.
- The IICS org you choose should be based on the home institution of the use case(s), regardless of the source, target, or scope of data being integrated.
- Cross-cutting use cases that span the System, e.g. for shared ERPs like Workday, should be housed in the System org.

## Examples

1. You are building an integration for a UW-Madison business unit whose application needs data about people and jobs from across UW System. You should use the UW-Madison IICS org and you will be supported by DoIT Enterprise Integration because your use case is for a UW-Madison business purpose. Despite the System-wide scope of the data you're integrating, Madison funds usage and support of IICS for you and your business unit.

1. You work in a UW-Madison business unit (e.g. Enterprise Business Systems) but are doing work funded by and on behalf of UW System (e.g. building integrations for Workday). Because the use case is for a UW System-wide business purpose, you should use the UW System IICS org.

1. Consider a circumstance in which UW-Madison decides to change our Integration Platform vendor. While we have no plans to do this today, the impact of that decision should be limited to the scope of UW-Madison use cases and business units, not System use cases (e.g. Workday or UW System data lakes). If such a change would impact the ability to provide services to other institutions or across UW System, then that integration use case should use UW System IICS org.

## UW System IICS Contact and Support

If you have determined that you should use the UW System IICS org, please contact [Justin Weidner](mailto:justin.weidner@wisconsin.edu). You may also obtain support from the UW System-wide Integration Platform Community of Practice by emailing [iPaaS-community-Practice@lists.wisconsin.edu](mailto:iPaaS-community-Practice@lists.wisconsin.edu).
