# What Are Supervisory Organizations?
For an explanation on Supervisory Organizations, please read the ATP Worday Organizations page [here](https://secure.atp.wisconsin.edu/workday-organizations/). The following tutorial will show the simple way we can pull Supervisory Organizations data from the HR API's [supervisoryOrganizations endpoint](https://developer.wisc.edu/docs/api-team-hr-api/1/routes/supervisoryOrganizations/get) and deliver it in a .csv format for consumption in processes downstream.

# Prerequisites

- An app registered in the [Developer Portal](https://developer.wisc.edu/get-started).
- Approval of that app for access to the [HR API](https://developer.wisc.edu/docs/api-team-hr-api/1/overview).

# Create a Swagger File and host it

- Here is a document previously written about [creation of swagger files](https://git.doit.wisc.edu/interop/iics/external-iics-docs/-/blob/master/docs/tutorials/API_Related/PersonAPI/PersonAPIwRESTv2.md?ref_type=heads#creating-your-own-swagger-files) for reference.
- If you need a place to publically host your swagger file, you can use the [Respository](https://git.doit.wisc.edu/interop/iics/iics-swagger-file-public-repository) that the Integration Team has set up for this purpose.

1) The first step to create this swagger file will be to use [Postman](https://www.postman.com/product/api-client/) or a similar API client with your Developer Portal's registered App's credentials to save a response for the HR API's [supervisoryOrganizations endpoint](https://developer.wisc.edu/docs/api-team-hr-api/1/routes/supervisoryOrganizations/get).

![JsonResponse](images/suporg1.png)

2) Next, head to Informatica.  You'll want to be in the Administrator section, then click "Swagger Files" in the left column.
3) Click "New" in the upper right and fill in the fields as seen in the two screenshots below.
    - Create a name.
    - Select a runtime environment (the same Secure Agent you typically work from).
    - URL:  https://api.wisc.edu
    - Verb:  GET
    - Authentication Type:  None
    - API Base Path:  /hr
    - API Path:  /supervisoryOrganizations
    - Accept: application/json
    - Query Params:  **{"page[number]": 0}**
    - Operation Id: Get_SupOrgs (this can be anything you like, it's for your reference only)
    - JSON Response File: This is the file you created in Step 1 above.

![Swagger1](images/suporg2.png)
![Swagger2](images/suporg3.png)

4) Click "Save" in the upper right, then find the Swagger File you just created and download it.
5) You will need to host this file publicly. If you don't have your own repository to do this, you can use the EI Integration Team's [Swagger Repository](https://git.doit.wisc.edu/interop/iics/iics-swagger-file-public-repository). Please be mindful of our [organizational strategy](https://git.doit.wisc.edu/interop/iics/iics-swagger-file-public-repository/-/blob/main/file-organization-guidelines.md).

# Rest V2 connection

1) Click into the "Connections" section of Administrator.
2) Click "New Connection" in the upper right.
3) Fill the settings in as seen in the screenshot below.
    - Name your connection according to [Best Practices](https://git.doit.wisc.edu/interop/iics/external-iics-docs/-/blob/master/docs/best-practices/naming.md).
    - Type:  REST V2
    - Select a runtime environment (the same Secure Agent you typically work from).
    - Authentication: OAuth 2.0-Client Credentials
    - Access Token URL:  https://api.wisc.edu/oauth/token
    - Client ID:  Your ID from the Developer Portal for your registered app
    - Client Secret:  Your Secret from the Developer Portal for your registered app
    - Client Authentication:  Send Client Credentials In Body
    - Access Token:  Click the button for "Generate Access Token"
    - Swagger File Path:  If you host with the EI Integration Team's Repository, this file path will be the raw URL, obtained by clicking the "Open raw" button on the gitlab page of your Swagger File.
4) Test your connection and if it's successful, click "Save".

![RestV2](images/suporg4.png)

# The Mapping

This part you'll notice is quite straightforward. The only special attention we need to take is to the pagination settings in the Source.

1) Create a Mapping in the Data Integration section of Informatica.
2) In the Source tile, select the REST V2 connection you created in the previous section.
3) There should be only one option for Operation, which you named while creating the Swagger File in the Swagger section of this tutorial.
4) Expand the "Request Options" section and click "Configure...".
5) Adjust the "page[number]" to equal 1 and press "OK".
    - if you don't see text in the Request Message text box, you may need to expand the "Request Message Template" section and copy it down.

![ReqOptions](images/suporg5.png)

6) Expand the "Advanced" section in Source and adjust the settings to be like the screenshot below.
    - Paging Type:  Page
    - Page Parameter:  page[number]
    - Start Page:  1
    - End Page:  1000  
    - Page Increment Factor:  1
    - End Of Response Expression:  "pageSize":0  **(no spaces in this field)**
    - The rest of the settings remain at their default.

![Pagination](images/suporg6.png)

7) Click "Field Mapping" in the Source tile and select any data elements you'd like to pull out of the API.

![FieldMapping](images/suporg7.png)

8) In the Target tile, select any target you have set up that can receive a .csv file. You can select "Create New at Runtime" if you don't have headers set up yet if you prefer. You can also choose to further format this data to fit your needs before delivery to your target connection.