# Best Practices And Recommendations

Here are some recommended best practices for designing and working with integrations in IICS.

* [Working In a Shared Environment](/docs/best-practices/shared-environment.md)
* [Naming Conventions](/docs/best-practices/naming.md)
* [AWS S3 as a Flat file alternative](/docs/best-practices/flat-file-alternative.md)
* [Cloud Data Integration (CDI) vs. Cloud Application Integration (CAI)](/docs/best-practices/cai-vs-cdi.md)
* [Working With the Test and Prod IICS Orgs](/docs/best-practices/multiple-orgs.md)
* [Email Alerting](/docs/best-practices/email-alerting.md)
* [Firewall Expectations](/docs/best-practices/firewallexpectation.md)
* [SSH / SFTP Authentication](/docs/best-practices/SSH_SFTP_auth.md)
* [Troubleshooting User Agent Issues in APIs](/docs/best-practices/api-user-agent.md)