# Basic Requirements

To set up a secure agent on Linux, running in a Docker container, you will need several things:

- Access to a workstation with an Intel-based processor. This is needed to run the Secure Agent. This could be a work computer, or, in the case of someone using a Macbook with an M1 processor, this could be an Amazon WorkSpaces desktop.
- Git. This is needed to pull the necessary Secure Agent files. [Official instructions on installation can be found here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
- Docker. This is used to set up the container in which the Secure Agent will run.
- [Docker Compose](https://docs.docker.com/compose/). This is an additional command used to build the image which the Secure Agent runs. I[nstructions to install this and Docker on an AWS Workspace can be found here](https://www.cyberciti.biz/faq/how-to-install-docker-on-amazon-linux-2/)
- Your own IICS trial account. If you are already using the shared test or prod organizations, this tutorial is unnecessary. Use ei.secureagent.doit.wisc.edu instead. If you are using a different organization with a pre-existing Secure Agent, follow the instructions given to you when your access was provisioned.

# Setup

0. Open up the command line, in whatever directory you would like to be the parent directory of your Secure Agent folder.
1. Clone the Secure Agent repository from GitLab. Change the current working directory of your command line to the newly cloned folder. This will create a new folder in whatever directory you are working in from the command line:
```
    git clone https://git.doit.wisc.edu/interop/iics/iics_secure_agent
    cd iics_secure_agent
```
2. Switch to the ```production-override``` branch of the repository, and confirm you are on this branch:
```
    git checkout production-override
    git status
```
3. Copy the Environment Variable file, and the infaagent file:
```
    cp .env-example .env
    cp conf/infaagent-example.ini conf/infaagent.ini
```
- After doing this, open the Enviornment Variable file (.env), and add these lines:
```
    INFORMATICA_USER=[Your Informatica username]
    INFORMATICA_PASSWORD=[Your Informatica trial account password]
    HOSTNAME=your-hostname
    POD=usw5
    REGION=dm-us
```
- The user name and password are the username and password of the IICS trial account you are doing the training on.
- The hostname is the name of the secure agent. You can choose any name here, but note that if you are doing this on AWS WorkSpaces, it will be overridden in IICS.
- The pod and region are determined by looking at the URL of your IICS account. For example, if the URL was https<area>://usw5.dm-us.informaticacloud.com/…, the pod would be usw5 and the region would be dm-us.
4. Create shared volume and logs volume, for transferring files from the docker container to your workstation and vice-versa:
```
    mkdir -p volumes/log
    mkdir -p volumes/shared
    touch volumes/log/agentcore.log
    touch conf/odbc.ini
```
- Afterwards, change ownership permissions to allow the secure agent to write to the volumes (note that these commands specifically must be entered one at a time):
```
    sudo chown -R 1000:1000 volumes/shared/
    sudo chown -R 1000:1000 conf/*
    sudo chown -R 1000:1000 volumes/log/
```
5. Build the container:
```
    docker-compose build
```
# Running the Secure Agent
1. Run the secure agent, and find the Container ID:
```
    docker-compose up -d
    docker ps
```
- After finding the Container ID, use it to follow the logs:
```
    docker logs --follow <docker name or number>
```

2. Check your IICS account, in the Runtime Enviornment tab of the Administrator service. It could take some time for the Secure Agent to enable all services. An audit log is available to check.
3. To shut down the Secure Agent:
```
    docker stop <container ID>
```
- alternatively:
```
    docker kill <container ID>
```
# Accessing Secure Agent Filesystem
Type in this command:
```
    docker exec -ti <container ID> bash
```
This allows you to view the files in the Docker container through a command-line interface. This can be useful for checking issues with the various volumes that are shared between the Docker container and the desktop.
# Deleting Secure Agent for Fresh Install
1. Purge all Docker containers and images:
```
    docker kill <container ID>
    docker system prune -a  
```
- You will have to type 'y' after doing this to have the system prune go through.
2. Delete the iics_secure_agent folder.
3. Delete the Secure Agent on your IICS trial account. Note that you may first have to delete all connections and tasks using the group for this Secure Agent as a runtime environment.
# Notes
- These commands are designed to be run on a Linux machine, specifically Amazon Linux 2 on Amazon WorkSpaces desktops. Other Linux environments or operating systems may produce different results.
- As of August 2022, this is the only viable method to get a Secure Agent running on a Mac with an M1 chip. Stable virtualization and emulation options which support running a Secure Agent on macOS (e.g., Parallels, VMware Fusion) are not yet available for machines with Apple silicon processors.
- The Docker container can also be run by typing 'docker-compose up', but this will have it take up the command line with log tracking. Exiting will shut down the secure agent if run in this method, whereas using the '-d' flag will not.
- The Container ID of the Secure Agent will usually be either 'iics_secure_agent_iics-secure-agent-1' or 'iics_secure_agent_iics-secure-agent_1'.
