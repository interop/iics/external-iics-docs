# Sharepoint Online Connector

Sharepoint Online has a complicated set up and we do not recommend this for the regular user. Sharepoint Online should only be implemented if you are familiar with administering Sharepoint Sites and have Sharepoint administration permissions, which may require some communication with the UW Office 365 administration team via DoIT Help Desk.

This process may need to be repeated every 90 days as that is the Refresh Token's lifespan.  Informatica does not mention an automated refresh through their App Connector and also doesn't mention this token expiring, so planning for the manual process of retrieving a new Refresh Token after 90 days is the smart way to go.

## Prerequisites
- Existing Sharepoint Online Site
   - Instructions to create one can be followed with [Microsoft's tutorial here](https://support.microsoft.com/en-gb/office/create-a-site-in-sharepoint-4d1e11bf-8ddc-499d-b889-2b48d10b1ce8).
- Administrator access to a Sharepoint Online site.
- [Installation of Postman](https://www.postman.com/downloads/) or use of a free Postman account for their [web version](https://identity.getpostman.com/login).


## Create Client ID and Client Secret in Azure.
 
1) Browse to portal.azure.com and in the search bar, search "App reg". 
   - Click on _App Registration_.
2) Click _New registration_.
   - Add a Name for your app, preferably with your department name and "IICS" for easy searching later.
   - Select _Accounts in this organizational directory only (UW-Madison only - Single tenant)_.
   - In the "Reirect URI" section, select _Web_ in the drop menu.
   - Enter a URL in the next field. It should be simple and will be used in a later step for token verification, so save what you've entered.  We will use https://localhost/.
   - Click _Register_ at the bottom of the screen.

![AzureAppReg](images/AzureAppReg.png)
 
3) In your notes, copy the resulting _Application (client ID) ID_ and the _Directory (tenant) ID_.
   - The _Directory ID_ is also known as the Bearer Realm, in case you are following along in another site's tutorials.
4) In the left column of the page, click _Certificates & secrets_.
   - Click _+ New client secret_.
   - Add a description that is meaningful to you.
   - Use the drop menu to add a 2 year expiry (this is the upper limit).
   - Click _Add_.
5) On the resulting screen you will see your secret (listed as _Value_) as well as your _Secret ID_. Copy the _Value_ into your notes. 
   - This is the only time you'll see the secret and you will not be able to retrieve the secret once you close this browser window.


### **For following steps, we have made a [Postman Collection](Sharepoint Refresh Token.postman_collection.json) for you to easily plug your values in.**

## Get the Authorization Code
[Informatica Reference](https://knowledge.informatica.com/s/article/572464?language=en_US)

1) Prep! When you generate the Authorization Code, you will only have 5 minutes to use it to get the Refresh Token. It will help if you have this constructed beforehand so you can plug the code straight in.
2) In Postman, open a new scratch window and select POST (instead of GET from above)
3) Enter the URL: https://accounts.accesscontrol.windows.net/<directory_id>/tokens/OAuth/2 (notice you'll need to plug in your bearer realm)
4) Select the Headers tab and enter key "Content-Type" with the value of "application/x-www-form-urlencoded"
5) Next select the Body tab under the URL bar and select the "x-www-form-urlencoded" option.
6) Plug your values from your notes into this formula and plug that into the Body:
```
client_id=<application_id>@<directory_id>
scope=https://<yoursite>.sharepoint.com/.default offline_access
code=<auth_code>
redirect_uri=<redirect_url>
grant_type=authorization_code
client_secret=<client_secret>
resource=00000003-0000-0ff1-ce00-000000000000/<yoursite>.sharepoint.com@<DirectoryID>
```

This is how ours looks:
```
client_id=efc45312-15ab-4f9d-b4f6-c77f8cb78866@2ca68321-0eda-4908-88b2-424a8cb4b0f9
scope=https://uwprod.sharepoint.com/.default offline_access
code=<very long value we retrieve in the next step>
redirect_uri=https://localhost/  *(yes, this works with URL)*
grant_type=authorization_code
client_secret=KZIC86XXXXXXXXXXXXXXXXXXXXXXXXY%3D  (X'd out most for security)
resource=00000003-0000-0ff1-ce00-000000000000/uwprod.sharepoint.com@2ca68321-0eda-4908-88b2-424a8cb4b0f9
```
![PMTemplate](images/pmURLencodedtemplate.png)

7) Construct a URL in the following way:
```
https://<your_site.sharepoint.com>/<subsitedomain>/_layouts/15/OAuthAuthorize.aspx?client_id=<application_id_only>&scope=Web.Manage&response_type=code&redirect_uri=<redirect_uri_from_first_section>
```
   - you will need to use [this site](https://www.urlencoder.org/) (or any other URI converter) to change characters that aren't normally allowed in URLs for the <redirect_uri> portion of this constructed URL.
   - here is an example URL, notice the https://localhost/ has been converted at the end:
```
https://uwprod.sharepoint.com/sites/IICSRepo/_layouts/15/OAuthAuthorize.aspx?client_id=efc45312-15ab-4f9d-b4f6-c77f8cb78866&scope=Web.Manage&response_type=code&redirect_uri=https%3A%2F%2Flocalhost%2F
```
8) Make sure you're logged into your Sharepoint site as administrator, then either in that same browser tab or a new one, browse to the above URL.
   - when it loads, click the "Trust it" button.

    ![TrustIT](images/TrustIT.png)

**After clicking "Trust It" you will get a "This site can't be reached" error, however the URL contains your 5 minute temporary Authorization Code.**

 ![DidntLoadURL](images/DidntLoadURL.png)

Copy everything from this URL **AFTER** "code=" and plug this code into the body of your Postman formula above.

9) Press Send, save the Refresh Token.

    ![SuccessPM](images/PostmanSuccess.png)
## Create Informatica Sharepoint Online Connector

1) In the Administrator section of Informatica, select Connections.
2) Select New Connection in the upper right. 
3) Name your connection with an abbreviation of your team in the title for convenient use.
4) The client secret in this connection does NOT need to be converted to URI format.
5) The URL field CANNOT have an ending forward slash.
6) If your Sharepoint site is a subsite, it needs to have the subsite URL end as shown below.

![SuccessfulConnection](images/successfulconnection.png)

