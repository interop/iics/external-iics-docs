# Firewall Expectations

For a database to successfully be accessed by IICS, it must first accept connections from the IICS Secure Agent. If the database has a firewall associated with it, then any attempt at connecting without altering the firewall will most likely result in an error (only providing the vague error message of ‘Connection Refused’ within IICS).

For connection to be successful, the database admin must edit the firewall to allow communications from the Secure Agent through. There are two secure agents running under the label ei.secureagent.wisc.edu - one for the test organization, and one for production - and each one has a separate IP address. These addresses are listed below.

|Organization|IP|
|:-------|:---------------|
|test | 3.230.240.5 |
|production | 3.19.12.147 |

Once the connection in IICS and the firewall rules in the database are set up, they can easily be tested by using the ‘test connection’ button in IICS. If the connection is unsuccessful, it is recommended you double-check the connection properties within IICS, and check the firewall traffic in the database to make sure no connection attempts are being blocked.

Note that there is an additional layer of firewalls contained within edge routers that encompasses all inbound traffic to the UW system. When using certain ports (notably 1433), the traffic will be blocked by the edge router. If you have determined this might be the case, you should contact the Integration Team at integration-platform@doit.wisc.edu for support.
