# Shared Orgs and Sub-Orgs

Informatica Intelligent Cloud Services (IICS) uses the concept of an organization (an "org") as the highest level container for objects, which include assets, projects, connections.
Orgs also contain a unique set of users and user groups.
When on-boarding to IICS, a decision will have to be made on whether to use the existing org for UW-Madison, or use a new parent org with sub-orgs.

The purpose of this document is to help prepare for this decision.

## Organizational Hierarchy

IICS has a hierarchy structure to ensure separation between teams and departments. 

### Parent Orgs

A parent org is the highest level concept in IICS. A parent org can contain 1-to-many sub-orgs and user groups.

### Sub-Orgs

A sub-org is functionally equivalent to a parent org, except some aspects are inherited from the parent org.
For example, a sub-org inherits licenses and connectors from the parent org.
Parent org secure agent groups can also be shared to sub-orgs.

Integrations can be monitored from the parent org to all sub-orgs, allowing the ability to get operational information for all environments in a single view.

A sub-org can not contain a sub-org. A sub-org can be created from within a parent org, or an existing org can be linked to a parent org to convert it into a sub-org.

### User Group

A user group allows further separation/isolation within an org.
If fine-grained authorization is needed within an org, a user group can be applied to objects (e.g. assets, connections, secure agents) to limit access to a subset of org users.

If no user group permissions are applied to an object, everyone within the org can access it.

User groups cannot contain other user groups.

## Options

### Use the Shared Org

This is the most commonly used option, and heavily reccomended if there is no specific need for a new org. For example, DCS, Admissions, SMPH, and DoIT-AIS all use the same org, sharing the development enviornment.

User Groups and Asset permissions are used to ensure that security between the groups is maintained. For more information on this concept, see the documentation on [Working In a Shared Environment](/docs/best-practices/shared-environment.md).

There is no extra cost to go this route, but there are some drawbacks to consider:

- User group permissions are not applied by default; Users must remember to apply user group permissions to their project or assets. If they do not, any user within the org can view, edit, or delete these assets.
- Connections and projects are visible in the list of all connections/projects, even if those objects have user group permissions on them.
As a result, you will see other departments' projects and connections when browsing in the user interface, or when selecting a connection from dropdown menus. To help delineate your objects from those of other teams, we recommend namespacing them. See the [Shared Enviornment Documentation](/docs/best-practices/shared-environment.md) for more information.
- There only two parent orgs: one for Test and one for Production. If more nuanced development enviornments (e.g. dev, QA, staging) are needed this can represent a drawback. Objects can be named or organized to indicate a specific environment (e.g. "dev-mapping", "dev-project"), but representing multiple development environments in a single org is not recommended. For reccomendations on how to use the default Test and Prod orgs, see the [Documentation on Multiple Orgs](/docs/best-practices/multiple-orgs.md).

### Use a Sub-Org

A sub-org is functionally equivalent to a parent org, except some aspects are inherited from the parent org.
For example, a sub-org inherits licenses and connectors from the parent org. Currently, [ODMAS](https://data.wisc.edu/) group is provisioned in this way.


![UW-Madison-IICS-org-hierarchy.](images/IICS-Organization-hierarchy.svg)

The diagram source can be found [here](https://app.lucidchart.com/documents/edit/5b0329b6-a058-49d8-a379-25d31f48d91a/0_0?beaconFlowId=0AB69376446D424F#?folder_id=home&browser=icon).

- Parent org secure agent groups can also be shared to sub-orgs.
- A sub-org cannot contain a sub-org. A sub-org can be created from within a parent org, or an existing org can be linked to a parent org to convert it into a sub-org.

Unlike using the shared org, using a sub-org approach has extra financial costs. Depending on your needs, the 
Integration Platform Team might be able to help cover the extra cost. Please [contact us](mailto:integration-platform@doit.wisc.edu) if this is something you would like to explore.

Here are some recommendation when to use a sub-org:
- Sub-orgs can be used for isolating different environments such as `test`, `qa` or `prod`. 
- If your integration requires more than few integration assets and connections, then a sub-org may be a better fit.
- If fine-grained authorization is needed within an sub-org, a user group can be applied to objects (e.g. assets, connections, secure agents) to limit access to a subset of sub-org users.
