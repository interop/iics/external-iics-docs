# IICS Support

Informatica provides a support mechanism for IICS issues ranging from enabling a connector or answering a technical question, to handling a production outage.
Due to license restrictions, the DoIT Integration Platform Team has limited ability to give university IICS users direct access to creating Informatica support cases.
Instead, the Integration Platform Team can open support cases on behalf of users.

## Opening a Support Case

These instructions apply to users who use either the shared IICS orgs or their own sub-orgs, except for those who have direct access to Informatica support.

* To open a support case with Informatica, email the Integration Platform Team ([integration-platform@doit.wisc.edu](mailto:integration-platform@doit.wisc.edu)). In some cases, we may be able to assist without opening a support case with Informatica. Please include the following information with your support request:
    * A summary of the problem.
    * Steps to reproduce the problem.
    * Any relevant logs or error messages.
    * Steps that have been used to troubleshoot (linking to the relevant Informatica documentation if possible).
    * The impact (how widespread is it, who does it affect?) and severity (how bad is the issue, are there workarounds?) of the issue. This will help us prioritize the issue and mark the appropriate priority with Informatica support.
* Except for production issues, we will respond within 2 business days.
* If a support case is needed, we will open one with Informatica and add you as a CC in the email thread. This will allow you to receive case updates and respond to cases directly.
* Wait for a response from Informatica. If they request a troubleshooting step that you are able to perform, you can respond to the case with the results of the troubleshooting or ask additional questions as needed.
* If Informatica support requests information regarding the shared secure agent, how the org is setup from an administrative perspective, or anything else beyond the scope of the integrations you manage, the Integration Platform Team will respond as needed.

## Escalating Support Cases

If you need to escalate a support case (for example, if an issue has grown to create production issues), please email the Integration Platform Team directly to request that a case is marked as the appropriate priority with Informatica support.
This does not apply for those who have direct access to Informatica support, since their access allows them to escalate cases from the support portal.

## Escalating Support Cases Beyond Informatica Support

If a support case or general Informatica issue needs to be escalated beyond Informatica support, please email the Integration Platform Team ([integration-platform@doit.wisc.edu](mailto:integration-platform@doit.wisc.edu)) directly.
We will escalate issues to our Informatica account contacts or to UW System as appropriate. UW-Madison IICS users must contact the Integration Platform team instead of contacting UW Shared Services directly.

This applies to all UW-Madison IICS users, regardless of whether they have direct access to create and manage support cases, or if they use the shared org or a sub-org.
