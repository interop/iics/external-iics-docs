# Training

**Important Note:** UW Madison has its own training offerings for learning IICS. If your primary goal is to gain a general overview of Cloud Data Integration and Cloud Application Integration, it is recommended that you follow the [University of Wisconsin Integrator Training](/docs/training/IntegratorTraining.md).

## Informatica Training

Informatica offers training to help beginner, intermediate, and advanced users take advantage of the various components of the IICS platform.
Most training is free, self-paced, and can be started at any time.
Instructor-led training is not free.
DoIT is currently not able to pay for instructor-led training but it is available for purchase.

Training is organized based on the component of IICS. 
Data Integration and Application Integration are the two main components of the IICS service that DoIT supports.

- Data Integration is meant for moving data in batches on a scheduled or ad-hoc basis.
Informatica also refers to Data Integration as Cloud Data Integration.
For example, you can use Data Integration to create integrations that copy data from one database to another or copy data from a database to a CSV file.
- Application Integration is meant to support real-time integrations that process data in an event-driven or transactional method.
You might also see Application Integration referred to as Cloud Application Integration (CAI).
Examples of integrations suited for Application Integration include a process that connects to ActiveMQ and processes events, or a web service that captures webhooks from a system and maps the data into a database.

## Registration

Informatica training requires you to register a free account.
Your account can access training material as well as documentation on [Informatica Network](https://network.informatica.com/).
The account is managed by Informatica and it is not possible to use NetID Login to access training material at this time.
You can delete your account at any time.

When signing up for training or accessing training materials, you will be asked to log in.
You can create an account if you don't already have one.

## Training Resources

There are two main services offered by Informatica to access training material: Informatica University and the Success Portal. We recommend using Informatica University for training and then using the Success Portal for supplemental and ongoing learning.

### Informatica University

[Informatica University](https://www.informatica.com/services-and-training/informatica-university/find-training.html) has the most training resources available.
Filter by product "Cloud Integration" and training method "onDemand" to browse the free self-paced trainings.

In addition, reccomended training programs can be found in the page for [Informatica University Cloud Training Paths](https://now.informatica.com/Cloud-Landing-Page.html).

For Data Integration, we recommend starting with the training "[IICS: Cloud Data Integration Services](https://now.informatica.com/IICS-Cloud-Data-Integration-Services-onDemand.html)".

For Application Integration, we recommend starting with the training "[Cloud Application Integration Processes for Developers](https://now.informatica.com/Cloud-Application-Integration-Services-for-Developers-onDemand.html)".

### Success Portal

Informatica offers video tutorials, how-to guides, and webinars through the [Success Portal](https://success.informatica.com/).
Under "Product Learning Path", select Cloud Application Integration or Cloud Data Integration.

Some areas of the Success Portal link out to [Informatica Network](https://network.informatica.com/), which includes FAQ articles, the Knowledge Base, product documentation, and user forums referred to as Communities.

## Secure Agents for Training

During some trainings, there might be mentions of setting up your own secure agent or accessing a secure agent directly. Information on Secure Agents and their usage can be found in [This Documentation](/docs/secure-agent.md).

If one is using their own trial organization for training purposes, secure agents can be created using [These Instructions](/docs/training/docker-secure-agent.md).

## Supplemental Training Information

For additional information regarding training, look at this [Supplemental Training Documentation](/docs/training/training-supplements.md).
