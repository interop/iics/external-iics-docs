# Dynamic file names are tricky

## Sources

### Amazon S3 Bucket

Amazon S3 buckets can use dynamically named sources through In-Out parameters. These can be assigned using taskflows.

1) In a Taskflow, create an assignment task and add a function.

![Assignmenttask](images/assigntask.png "AssignTask")

2) Add a template file to your S3 bucket that includes all headers and full example of the dynamic file name (for example, MyFile_20230512.csv). This file will need to remain in your bucket for the life of the integration/mapping.

![DynamicTemplate](images/dynatemplate.png "Dynamic Template")

3) Add into the **Mapping Task** source Advanced settings the input parameter from the Assignment task.

![InputPara](images/inputpara.png "Input Parameter")

## Targets

### Flat File

Flat Files are able to be named dynamically using the "Dynamic File Name" property when creating a new target at runtime. Official documentation on this process can be found [In This Document](https://docs.informatica.com/integration-cloud/data-integration/current-version/transformations/target-transformation/file-targets/flat-file-targets-with-dynamic-file-names.html).

Flat files can also be set up to output to a dynamic file name based on date in the same way an S3 bucket can. Refer to the section below.

### Amazon S3 Bucket

Amazon S3 connections can be set up to output to a dynamic file name based on the date using special characters. Official Documentation on this process can be found [In This Document](https://docs.informatica.com/integration-cloud/data-integration-connectors/current-version/amazon-s3-v2-connector/mappings-and-mapping-tasks-with-amazon-s3-v2/amazon-s3-v2-objects-in-mappings/amazon-s3-v2-target-file-parameterization/parameterization-using-timestamp.html).

![S3dynamicTarg](images/S3dynatarget.png "S3 Dynamic Target Name")

S3 Bucket targets can also be renamed via in-out parameters, similar to the process above used for dyanmic source filenames. Official documentation can be found [At This Page](https://knowledge.informatica.com/s/article/625545?language=en_US).

### Box

Box connections are unable to output dynamically named files of any kind. If dynamic file naming is an absolute requirement for your integration, use a different service to host target files.
