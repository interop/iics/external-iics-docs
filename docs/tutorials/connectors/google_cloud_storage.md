# Google Cloud Storage V2

Google Cloud Platform is the University of Wisconsin's [preferred provider](https://kb.wisc.edu/public-cloud/115296) for Sensitive and Restricted Data. Please read through [this KnowledgeBase article](https://kb.wisc.edu/page.php?id=110947) to see if Google Cloud Platform is a good fit for you and your team. We find this connector in IICS is very similar to the [Amazon S3 v2 connector](docs/tutorials/connectors/Amazon_S3_v2/Amazon_S3_v2.md).

# Use Case Example

A repository or target for any .csv transformation done within IICS. For instance, you may have an Oracle database as a source of person data but would like to adjust phone numbers to have parentheses instead of dashes. IICS can pull, transform, and drop this data as a .csv into a target Google Cloud bucket.

# Bucket Set up

1) Be sure to establish a Google Cloud account through the UW's [Cloud Platform Team](https://kb.wisc.edu/public-cloud/109785) and NOT with a personal or private Google account. Once you have done that, this is the Google Cloud [landing page](https://console.cloud.google.com/welcome).
2) Once you log in with your UW account, you should have *Cloud Storage* in your quick access. If you do not, you can also access it with the sidebar.

![CloudStorageMenu](images/CloudStorage.png)

3) Select *Buckets*, then select the *+ Create*.
    - we do not have recommendations on settings your bucket, we have used the default settings. Please read through them and pick what is right for your needs.

![CreateBucket](images/CreateBucket.png)

4) Now a service account is needed. In the sidebar, select *IAM & Admin*, then *Service Accounts*.

![googservice.png](images/googservice.png)

5) On the next page, select *+ Create Service Account*.
6) Create a name and description.  The Service account ID will be generated automatically. Select *Create and Continue*.
7) Select *Storage Object Admin* for the role.
8) Click the *+ Add AIM Condition* and a large sidebar will slide out from the right.
9) Enter a title of your condition and select the *Condition Editor* tab.
10) Copy and paste the following line into the condition editor, replacing *your-bucket-name* with the bucket we created in the previous steps.
```
resource.name.startsWith("projects/_/buckets/your-bucket-name")
```
11) Click *Save*.
12) Select *+ Add Another Role* and select *Storage Object User*.
    - This is to add the object.storage.list permissions that IICS needs for this user.
13) Repeat steps 8-11 for the Storage Object User role.
14) On the *Service accounts* page, click the Email link of the service account you've just created.
15) Select the *KEYS* tab of the service account and click *ADD KEY* > *Create new key*.
16) Select json as the *Key type* and click *Create*.

![create_key](images/create_key.png)

17) This will download a service account key file to your local machine and should be kept securely.

# Connector Example

All of the information you need for the connector properties are within the json file from the previous section. You should have the json file open in a text editor of your choice for this section.

1) Log into IICS, choose the Administrator section, and click *Connections*.
2) Select *New Connection* in the upper right of your browser window.
3) Create a team-specific title and follow best practices for your [naming convention](docs/best-practices/naming.md).
4) Select *Type* Google Cloud Storage V2 (Informatica).
5) Select the *Runtime Environment* you expect to use the connection from.
6) Enter these fields from your json file:
    - *Service Account ID* = *client_email* without the quotation marks at the beginning and end
    - *Service Account Key* = *private_key_id* without the quotation marks at the beginning and end
    - *Project ID* = *project_id*
7) Enter your *Bucket Name* in connection proproties.
8) Verify successful connection test.

![googjsonfile](images/googjsonfile.png)
![googconnection](images/googconnection.png)