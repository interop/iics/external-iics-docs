# Integration Tutorials
Linked below are several walkthroughs on Informatica objects and processes used in creating Integrations. If you are just starting to create integrations and looking for a jumping-off point, it is recommended you follow the [University of Wisconsin Integrator Training](/docs/training/IntegratorTraining.md) instead.

* [Basic Extract, Transform, Load (ETL) integration example for WiscAlerts](/docs/tutorials/wiscalerts/wiscalerts.md)
* [File Retrieval from the IICS Secure Agent](/docs/tutorials/FileRetrieval/FileRetrieval.md)
* [Using the Box Connector For Flat Files](/docs/tutorials/box.md)
* [Webhooks in Cloud Application Integration (CAI)](/docs/tutorials/PersonAPI/Webhooks.md)
* [Dynamic File Names](/docs/tutorials/DynamicFileNames/DynamicFileNames.md)
* [Sharepoint Connector](/docs/tutorials/connectors/sharepoint_online.md)
* [Amazon S3 v2 Connector](/docs/tutorials/connectors/Amazon_S3_v2.md)
* [Google Cloud Storage V2 Connector](/docs/tutorials/connectors/google_cloud_storage.md)
* [FTP/SFTP Connector](/docs/tutorials/connectors/FTP_connector.md)
* [File Processor Connector for SSH keypair authentication](/docs/tutorials/connectors/SSH_key_file_processor.md)