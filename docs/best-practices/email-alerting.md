# Email Alerting

It's important to know when your integration fails and why.  If no email is entered, no one will be alerted that an integration fails and it may be very inconvenient if things start falling apart. 

## Tutorial

Informatica has created a [knowledgebase article located here](https://knowledge.informatica.com/s/article/496185?language=en_US) to help you through the process in adding your email into your tasks and task flows.

## Confirmation

Success alerts can also be configured in this way, letting you know your integration ran without issue.
