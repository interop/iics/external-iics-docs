# EI Secure Agents

The EI Secure Agents are the specific secure agents that are used by most users of the UW Madison ecosystem. For more generalized information about secure agents and their standard implementation, please refer to [This Page](docs/secure-agent.md).

## General Information

The EI Secure Agents are the two agents named ei.secureagent.doit.wisc.edu, located in the standard Test and Prod orgs of IICS. The agents can be used by any user of the org, and is the most commonly used agent within the org. Each agent is located on a priavte EC2 instance so files can only be accessed through IICS functionality. In addition, any files hosted on the secure agent are potentially visible to any user of the secure agent. If privacy is a requirement, consider hosting any permanent files on external sources such as an Amazon S3 bucket.

## Technical Information

The EI Secure Agents are deployed in a docker container on two separate AWS EC2 instances in private subnets. A gitlab repo of the dockerfiles and terraform used to deploy the agent can be found at [This Repositiory](https://git.doit.wisc.edu/interop/iics/iics_secure_agent).


Due to the nature of a Docker container, files on the secure agents may be volatile, and if the secure agent restarts the file may be lost. If your integration needs to have non-volatile flat files, contact the Integration Team at integration-platform@doit.wisc.edu

The IP adresses of the Secure Agents (to be used for allowlisting database/server connections) are as follows:
| Org        | IP address |
| -----------|-------------|
| test      | 3.230.240.5|
| prod      | 3.19.12.147|
