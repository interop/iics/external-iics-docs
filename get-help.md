# Get Help

If you weren't able to answer your question using the documentation in this repository, here are some options that are available to get help.

- IICS access must be provisioned before the service can be used. If you are UW Madison Personell seeking to access our IICS offerings, please look at our [Onboarding Page](/docs/on-boarding-to-iics.md)
- If you are looking to learn how to use IICS and what a basic use case looks like, please look at our tutorial on [Creating your First Integration](/docs/tutorials/FirstIntegration/FirstIntegration.md).
- IICS provides its own offerings regarding training. To access this training, either click the links below or look at our [Training Documentation](./docs/training/training.md).
    - [Cloud Data Integration Training](https://onlinehelp.informatica.com/IICS/prod/CDI/en/index.htm)
    - [Cloud Application Integration Training](https://onlinehelp.informatica.com/IICS/prod/CAI/en/index.htm)
- Frequently asked questions are documented in the [Informatica Knowledge Base](https://search.informatica.com/).
- More specified pieces of documentation can be found at the [Informatica Documentation Page](https://docs.informatica.com/).
- [Informatica Network](https://network.informatica.com/) has a "Communities" section that allows anyone to create public forum posts to elicit help from other IICS users or experts. An Informatica Network account is required to use this feature. The account isn't associated with a NetID and can be deleted at any time.
- UW System has an integration platform community of practice made up of integrators who use IICS. Questions, ideas, practices, and any other form of integration platform content can be shared with the community via their mailing list: [ipaas-community-practice@lists.wisconsin.edu](mailto:ipaas-community-practice@lists.wisconsin.edu)
- UW-Madison's Informatica license provides an ["Ask an Expert"](https://knowledge.informatica.com/s/article/new-feature-announcement-ask-an-expert?language=en_US) service, which allows anyone to get on a call with an Informatica expert to discuss design and architectural questions. Please contact [integration-platform@doit.wisc.edu](mailto:integration-platform@doit.wisc.edu) to schedule a session.
- Support cases can be created with Informatica for general usage questions enabling new connectors, or operational issues. Refer to [this document for more information on creating support cases with Informatica support](./docs/support.md).
- For all other questions related to UW-Madison's instance of IICS, please contact the DoIT Integration Platform team: [integration-platform@doit.wisc.edu](mailto:integration-platform@doit.wisc.edu)

